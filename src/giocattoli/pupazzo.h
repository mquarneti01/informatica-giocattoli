/* informatica-giocattoli
 *
 * Copyright (C) 2018  Claudio Giordani, Manuel Quarneti, Pietro Pezzi, Raffaele Ceroni
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
Pupazzo Sottoclasse di Giocattolo.
Attributi: numOcchi, numArti;
Metodi: getNumOcchi(), setNumOcchi(), getNumArti(), setNumArti();
Costruttore e Distruttore
*/
#ifndef PUPAZZO_H
#define PUPAZZO_H

#include "giocattolo.h"

class Pupazzo : public Giocattolo {
protected:
    int numOcchi;
    int numArti;

public:
    Pupazzo();
    ~Pupazzo();

    int getNumOcchi();
    void setNumOcchi(int);

    int getNumArti();
    void setNumArti(int);
};

#endif // PUPAZZO_H
