/* informatica-giocattoli
 *
 * Copyright (C) 2018  Claudio Giordani, Manuel Quarneti, Pietro Pezzi, Raffaele Ceroni
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
Superclasse Giocattolo
Attributi: primoMateriale, secondoMateriale, terzoMateriale, dimensione, costoProduzione, prezzoVendita;
Metodi: getprimoMateriale(), setprimoMateriale(), getsecondoMateriale(), setsecondoMateriale(),
getterzoMateriale(), setterzoMateriale(), getdimensione(), setdimensione(), getcostoProduzione(),
setcostoProduzione(), getprezzoVendita(), setprezzoVendita();
Costruttore e Distruttore
*/
#ifndef GIOCATTOLO_H
#define GIOCATTOLO_H

#include <string>

using std::string;

class Giocattolo {
protected:
    string primoMateriale;
    string secondoMateriale;
    string terzoMateriale;
    string dimensione; // G (grande), M (medio), (P) piccolo
    float costoProduzione;
    float prezzoVendita;

public:
    Giocattolo();
    ~Giocattolo();

    // getter e setter
    string getPrimoMateriale();
    void setPrimoMateriale(string);

    string getSecondoMateriale();
    void setSecondoMateriale(string);

    string getTerzoMateriale();
    void setTerzoMateriale(string);

    string getDimensione();
    void setDimensione(string);

    float getCostoProduzione();
    void setCostoProduzione(float);

    float getPrezzoVendita();
    void setPrezzoVendita(float);
};

#endif // GIOCATTOLO_H
