/* informatica-giocattoli
 *
 * Copyright (C) 2018  Claudio Giordani, Manuel Quarneti, Pietro Pezzi, Raffaele Ceroni
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
Barca Sottoclasse di Modellino.
Attributi: numVele , finiture;
Metodi: getNumVele(), setNumVele(), getFiniture(), setFiniture();
Costruttore e Distruttore
*/
#ifndef BARCA_H
#define BARCA_H

#include "modellino.h"

class Barca : public Modellino {
protected:
    int numVele;
    string finiture; //Nessuna,Remi,Oblò e ancore

public:
    Barca();
    ~Barca();

    int getnumVele();
    void setnumVele(int);

    string getFiniture();
    void setFiniture(string);
};

#endif //BARCA_H
