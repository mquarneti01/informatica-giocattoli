/* informatica-giocattoli
 *
 * Copyright (C) 2018  Claudio Giordani, Manuel Quarneti, Pietro Pezzi, Raffaele Ceroni
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
Puzzòa Sottoclasse di Giocattolo.
Attributi: numPezzi, soggetto;
Metodi: getNumPezzi(), setNumPezzi(), getSoggetto(), setSoggetto();
Costruttore e Distruttore
*/
#ifndef PUZZLE_H
#define PUZZLE_H

#include "giocattolo.h"

class Puzzle : public Giocattolo {
protected:
    int numPezzi;
    string soggetto;

public:
    Puzzle();
    ~Puzzle();

    int getnumPezzi();
    void setnumPezzi(int);

    string getSoggetto();
    void setSoggetto(string);
};

#endif //PUZZLE_H
