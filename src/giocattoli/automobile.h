/* informatica-giocattoli
 *
 * Copyright (C) 2018  Claudio Giordani, Manuel Quarneti, Pietro Pezzi, Raffaele Ceroni
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
Automobile Sottoclasse di Modellino.
Attributi: numRuote, colore;
Metodi: getNumRuote(), setNumRuote(), getColore(), setColore();
Costruttore e Distruttore
*/
#ifndef AUTOMOBILE_H
#define AUTOMOBILE_H

#include "modellino.h"
#include <string>

using std::string;

class Automobile : public Modellino {
protected:
    int numRuote;
    string colore;

public:
    Automobile();

    int getNumRuote();
    void setNumRuote(int);

    string getColore();
    void setColore(string);

    ~Automobile();
};

#endif // AUTOMOBILE_H
