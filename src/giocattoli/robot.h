/* informatica-giocattoli
 *
 * Copyright (C) 2018  Claudio Giordani, Manuel Quarneti, Pietro Pezzi, Raffaele Ceroni
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
Robot Sottoclasse di Pupazzo.
Attributi: radiocomandato;
Metodi: getRadiocomandato(), setRadiocomandato();
Costruttore e Distruttore
*/
#ifndef ROBOT_H
#define ROBOT_H

#include "pupazzo.h"

class Robot : public Pupazzo {
protected:
    bool radiocomandato;

public:
    Robot();
    ~Robot();

    bool getRadiocomandato();
    void setRadiocomandato(bool);
};

#endif // ROBOT_H
