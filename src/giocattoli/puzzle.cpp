/* informatica-giocattoli
 *
 * Copyright (C) 2018  Claudio Giordani, Manuel Quarneti, Pietro Pezzi, Raffaele Ceroni
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "puzzle.h"

Puzzle::Puzzle() {}

int Puzzle::getnumPezzi() { return numPezzi; }
void Puzzle::setnumPezzi(int numPezzi) { this->numPezzi = numPezzi; }

string Puzzle::getSoggetto() { return soggetto; }
void Puzzle::setSoggetto(string soggetto) { this->soggetto = soggetto; }

Puzzle::~Puzzle() {}
