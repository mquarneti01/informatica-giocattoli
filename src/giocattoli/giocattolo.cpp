/* informatica-giocattoli
 *
 * Copyright (C) 2018  Claudio Giordani, Manuel Quarneti, Pietro Pezzi, Raffaele Ceroni
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "giocattolo.h"

Giocattolo::Giocattolo() {}
Giocattolo::~Giocattolo() {}

string Giocattolo::getPrimoMateriale() { return primoMateriale; }
void Giocattolo::setPrimoMateriale(string primoMateriale) { this->primoMateriale = primoMateriale; }

string Giocattolo::getSecondoMateriale() { return secondoMateriale; }
void Giocattolo::setSecondoMateriale(string secondoMateriale) { this->secondoMateriale = secondoMateriale; }

string Giocattolo::getTerzoMateriale() { return terzoMateriale; }
void Giocattolo::setTerzoMateriale(string terzoMateriale) { this->terzoMateriale = terzoMateriale; }

string Giocattolo::getDimensione() { return dimensione; }
void Giocattolo::setDimensione(string dimensione) { this->dimensione = dimensione; }

float Giocattolo::getCostoProduzione() { return costoProduzione; }
void Giocattolo::setCostoProduzione(float costoProduzione) { this->costoProduzione = costoProduzione; }

float Giocattolo::getPrezzoVendita() { return prezzoVendita; }
void Giocattolo::setPrezzoVendita(float prezzoVendita) { this->prezzoVendita = prezzoVendita; }
