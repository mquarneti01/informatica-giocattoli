/* informatica-giocattoli
 *
 * Copyright (C) 2018  Claudio Giordani, Manuel Quarneti, Pietro Pezzi, Raffaele Ceroni
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CONFIG_LOADER_H
#define CONFIG_LOADER_H

#include "json.hpp"
#include <fstream>
#include <iostream>

using namespace std;
using json = nlohmann::json;

class ConfigLoader {
private:
    json j;

public:
    ConfigLoader();

    int getNumPeluches();
    int getNumBambole();
    int getNumRobots();
    int getNumAutomobili();
    int getNumAeromodelli();
    int getNumBarche();

    int getMaxArti();
    int getMaxOcchi();
    int getMinRuote();
    int getMaxRuote();
    int getMinAli();
    int getMaxAli();
    int getMaxVele();
    float getMaxCostoProduzione();
    float getMaxPrezzoVendita();

    int getDimensioniSize();
    string getDimensione(int);

    int getMaterialiSize();
    string getMateriale(int);

    int getColoriSize();
    string getColore(int);

    int getVestitiSize();
    string getVestito(int);

    int getFinitureSize();
    string getFiniture(int);

    ~ConfigLoader();
};

#endif // CONFIG_LOADER_H
