/* informatica-giocattoli
 *
 * Copyright (C) 2018  Claudio Giordani, Manuel Quarneti, Pietro Pezzi, Raffaele Ceroni
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "giocattoli_random.h"

/* GENERA ATTRIBUTI RANDOM */

string randomMateriale(ConfigLoader config_loader)
{
    int materiali_size = config_loader.getMaterialiSize();
    int indice = rand() % materiali_size;
    return config_loader.getMateriale(indice);
}

string randomDimensione(ConfigLoader config_loader)
{
    int dimensioni_size = config_loader.getDimensioniSize();
    int indice = rand() % dimensioni_size;
    return config_loader.getDimensione(indice);
}

void randomCostoPrezzo(ConfigLoader config_loader, float* costo_e_prezzo)
{
    // imposta il costo di produzione
    float max_costo_produzione = config_loader.getMaxCostoProduzione();
    costo_e_prezzo[0] = (float)rand() / (float)(RAND_MAX / max_costo_produzione);

    // imposta il prezzo di vendita
    float max_prezzo_vendita = config_loader.getMaxPrezzoVendita();
    costo_e_prezzo[1] = (float)rand() / (float)(RAND_MAX / max_prezzo_vendita);

    // si assicura che il prezzo non sia minore del costo
    if (costo_e_prezzo[1] <= costo_e_prezzo[0])
        costo_e_prezzo[1] = costo_e_prezzo[0];
}

int randomNumArti(ConfigLoader config_loader)
{
    return rand() % (config_loader.getMaxArti() + 1);
}

int randomNumOcchi(ConfigLoader config_loader)
{
    return rand() % (config_loader.getMaxOcchi() + 1);
}

int randomNumRuote(ConfigLoader config_loader)
{
    int ruote = rand() % (config_loader.getMaxRuote() + 1);

    // se il numero di ruote non supera il minimo, gli somma quest'ultimo
    if (ruote < config_loader.getMinRuote())
        ruote += config_loader.getMinRuote();

    return ruote;
}

int randomNumAli(ConfigLoader config_loader)
{
    int ali = rand() % (config_loader.getMaxAli() + 1);

    // se il numero di ali non supera il minimo, gli somma quest'ultimo
    if (ali < config_loader.getMinAli())
        ali += config_loader.getMinAli();

    return ali;
}

int randomNumVele(ConfigLoader config_loader)
{
    return rand() % (config_loader.getMaxVele() + 1);
}

bool randomBool()
{
    int random_int = rand() % 2;
    return (bool)random_int;
}

string randomColore(ConfigLoader config_loader)
{
    int colori_size = config_loader.getColoriSize();
    int indice = rand() % colori_size;
    return config_loader.getColore(indice);
}

string randomVestito(ConfigLoader config_loader)
{
    int vestiti_size = config_loader.getVestitiSize();
    int indice = rand() % vestiti_size;
    return config_loader.getVestito(indice);
}

string randomFiniture(ConfigLoader config_loader)
{
    int finiture_size = config_loader.getFinitureSize();
    int indice = rand() % finiture_size;
    return config_loader.getFiniture(indice);
}

/* GENERA GIOCATTOLI RANDOM BASANDOSI SULLE FUNZIONI DEFINITE IN PRECEDENZA */

Peluche randomPeluche()
{
    ConfigLoader config_loader;

    Peluche peluche;

    peluche.setHaCoda(randomBool());
    peluche.setNumArti(randomNumArti(config_loader));
    peluche.setNumOcchi(randomNumOcchi(config_loader));
    peluche.setSecondoMateriale(randomMateriale(config_loader));
    peluche.setTerzoMateriale(randomMateriale(config_loader));
    peluche.setDimensione(randomDimensione(config_loader));

    float costo_e_prezzo[2];
    randomCostoPrezzo(config_loader, costo_e_prezzo);

    peluche.setCostoProduzione(costo_e_prezzo[0]);
    peluche.setPrezzoVendita(costo_e_prezzo[1]);

    return peluche;
}

Bambola randomBambola()
{
    ConfigLoader config_loader;

    Bambola bambola;

    bambola.setColoreCapelli(randomColore(config_loader));
    bambola.setVestito(randomVestito(config_loader));
    bambola.setPrimoMateriale(randomMateriale(config_loader));
    bambola.setSecondoMateriale(randomMateriale(config_loader));
    bambola.setTerzoMateriale(randomMateriale(config_loader));
    bambola.setDimensione(randomDimensione(config_loader));

    float costo_e_prezzo[2];
    randomCostoPrezzo(config_loader, costo_e_prezzo);

    bambola.setCostoProduzione(costo_e_prezzo[0]);
    bambola.setPrezzoVendita(costo_e_prezzo[1]);

    return bambola;
}

Robot randomRobot()
{
    ConfigLoader config_loader;

    Robot robot;

    robot.setRadiocomandato(randomBool());
    robot.setNumArti(randomNumArti(config_loader));
    robot.setNumOcchi(randomNumOcchi(config_loader));
    robot.setTerzoMateriale(randomMateriale(config_loader));
    robot.setDimensione(randomDimensione(config_loader));

    float costo_e_prezzo[2];
    randomCostoPrezzo(config_loader, costo_e_prezzo);

    robot.setCostoProduzione(costo_e_prezzo[0]);
    robot.setPrezzoVendita(costo_e_prezzo[1]);

    return robot;
}

Automobile randomAutomobile()
{
    ConfigLoader config_loader;

    Automobile automobile;

    automobile.setNumRuote(randomNumRuote(config_loader));
    automobile.setColore(randomColore(config_loader));
    automobile.setRadiocomandato(randomBool());
    automobile.setPrimoMateriale(randomMateriale(config_loader));
    automobile.setSecondoMateriale(randomMateriale(config_loader));
    automobile.setTerzoMateriale(randomMateriale(config_loader));
    automobile.setDimensione(randomDimensione(config_loader));

    float costo_e_prezzo[2];
    randomCostoPrezzo(config_loader, costo_e_prezzo);

    automobile.setCostoProduzione(costo_e_prezzo[0]);
    automobile.setPrezzoVendita(costo_e_prezzo[1]);

    return automobile;
}

Aeromodello randomAeromodello()
{
    ConfigLoader config_loader;

    Aeromodello aeromodello;

    aeromodello.setNumAli(randomNumAli(config_loader));
    aeromodello.setRadiocomandato(randomBool());
    aeromodello.setPrimoMateriale(randomMateriale(config_loader));
    aeromodello.setSecondoMateriale(randomMateriale(config_loader));
    aeromodello.setTerzoMateriale(randomMateriale(config_loader));
    aeromodello.setDimensione(randomDimensione(config_loader));

    float costo_e_prezzo[2];
    randomCostoPrezzo(config_loader, costo_e_prezzo);

    aeromodello.setCostoProduzione(costo_e_prezzo[0]);
    aeromodello.setPrezzoVendita(costo_e_prezzo[1]);

    return aeromodello;
}

Barca randomBarca()
{
    ConfigLoader config_loader;

    Barca barca;

    barca.setnumVele(randomNumVele(config_loader));
    barca.setFiniture(randomFiniture(config_loader));
    barca.setRadiocomandato(randomBool());

    barca.setPrimoMateriale(randomMateriale(config_loader));
    barca.setSecondoMateriale(randomMateriale(config_loader));
    barca.setTerzoMateriale(randomMateriale(config_loader));
    barca.setDimensione(randomDimensione(config_loader));

    float costo_e_prezzo[2];
    randomCostoPrezzo(config_loader, costo_e_prezzo);

    barca.setCostoProduzione(costo_e_prezzo[0]);
    barca.setPrezzoVendita(costo_e_prezzo[1]);

    return barca;
}
