/* informatica-giocattoli
 *
 * Copyright (C) 2018  Claudio Giordani, Manuel Quarneti, Pietro Pezzi, Raffaele Ceroni
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "aeromodello.h"
#include "automobile.h"
#include "bambola.h"
#include "barca.h"
#include "config_loader.h"
#include "peluche.h"
#include "robot.h"

string randomMateriale(ConfigLoader);
string randomDimensione(ConfigLoader);
void randomCostoPrezzo(ConfigLoader, float*);
int randomNumArti(ConfigLoader);
int randomNumOcchi(ConfigLoader);
int randomNumRuote(ConfigLoader);
int randomNumAli(ConfigLoader);
int randomNumVele(ConfigLoader);
bool randomBool();
string randomColore(ConfigLoader);
string randomVestito(ConfigLoader);
string randomFiniture(ConfigLoader);

Peluche randomPeluche();
Bambola randomBambola();
Robot randomRobot();
Automobile randomAutomobile();
Aeromodello randomAeromodello();
Barca randomBarca();
