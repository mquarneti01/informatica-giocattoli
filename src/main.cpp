/* informatica-giocattoli
 *
 * Copyright (C) 2018  Claudio Giordani, Manuel Quarneti, Pietro Pezzi, Raffaele Ceroni
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "giocattoli_random.h"

int main()
{
    // imposta il generatore di numeri casuali
    srand((unsigned)time(NULL));

    // inizializza il loader del file di configurazione
    ConfigLoader config_loader;

    // prende le quantita' di giocattoli da generare
    int num_peluches = config_loader.getNumPeluches();
    int num_bambole = config_loader.getNumBambole();
    int num_robots = config_loader.getNumRobots();
    int num_automobili = config_loader.getNumAutomobili();
    int num_aeromodelli = config_loader.getNumAeromodelli();
    int num_barche = config_loader.getNumBarche();

    // istanziazione json che sara' dato in output
    json to_dump;

    // generazione e memorizzazione dei peluches
    to_dump["Peluches"] = json::array();
    for (int i = 0; i < num_peluches; i++) {
        Peluche peluche = randomPeluche();

        to_dump["Peluches"][i] = {
            { "Materiali", { peluche.getPrimoMateriale(), peluche.getSecondoMateriale(), peluche.getTerzoMateriale() } },
            { "Dimensione", peluche.getDimensione() },
            { "Costo di produzione", peluche.getCostoProduzione() },
            { "Prezzo di vendita", peluche.getPrezzoVendita() },
            { "Guadagno", (peluche.getPrezzoVendita() - peluche.getCostoProduzione()) },
            { "Numero di arti", peluche.getNumArti() },
            { "Numero di occhi", peluche.getNumOcchi() },
            { "Ha una coda?", peluche.getHaCoda() }
        };
    }

    // generazione e memorizzazione delle bambole
    to_dump["Bambole"] = json::array();
    for (int i = 0; i < num_bambole; i++) {
        Bambola bambola = randomBambola();

        to_dump["Bambole"][i] = {
            { "Materiali", { bambola.getPrimoMateriale(), bambola.getSecondoMateriale(), bambola.getTerzoMateriale() } },
            { "Dimensione", bambola.getDimensione() },
            { "Costo di produzione", bambola.getCostoProduzione() },
            { "Prezzo di vendita", bambola.getPrezzoVendita() },
            { "Guadagno", (bambola.getPrezzoVendita() - bambola.getCostoProduzione()) },
            { "Numero di arti", bambola.getNumArti() },
            { "Numero di occhi", bambola.getNumOcchi() },
            { "Colore dei capelli", bambola.getColoreCapelli() },
            { "Vestito", bambola.getVestito() }
        };
    }

    // generazione e memorizzazione dei robot
    to_dump["Robots"] = json::array();
    for (int i = 0; i < num_robots; i++) {
        Robot robot = randomRobot();

        to_dump["Robots"][i] = {
            { "Materiali", { robot.getPrimoMateriale(), robot.getSecondoMateriale(), robot.getTerzoMateriale() } },
            { "Dimensione", robot.getDimensione() },
            { "Costo di produzione", robot.getCostoProduzione() },
            { "Prezzo di vendita", robot.getPrezzoVendita() },
            { "Guadagno", (robot.getPrezzoVendita() - robot.getCostoProduzione()) },
            { "Numero di arti", robot.getNumArti() },
            { "Numero di occhi", robot.getNumOcchi() },
            { "Ha un radiocomando?", robot.getRadiocomandato() }
        };
    }

    // generazione e memorizzazione delle automobili
    to_dump["Automobili"] = json::array();
    for (int i = 0; i < num_automobili; i++) {
        Automobile automobile = randomAutomobile();

        to_dump["Automobili"][i] = {
            { "Materiali", { automobile.getPrimoMateriale(), automobile.getSecondoMateriale(), automobile.getTerzoMateriale() } },
            { "Dimensione", automobile.getDimensione() },
            { "Costo di produzione", automobile.getCostoProduzione() },
            { "Prezzo di vendita", automobile.getPrezzoVendita() },
            { "Guadagno", (automobile.getPrezzoVendita() - automobile.getCostoProduzione()) },
            { "Ha un radiocomando?", automobile.getRadiocomandato() },
            { "Numero di ruote", automobile.getNumRuote() },
            { "Colore della carrozzeria", automobile.getColore() }
        };
    }

    // generazione e memorizzazione degli aeromodelli
    to_dump["Aeromodelli"] = json::array();
    for (int i = 0; i < num_aeromodelli; i++) {
        Aeromodello aeromodello = randomAeromodello();

        to_dump["Aeromodelli"][i] = {
            { "Materiali", { aeromodello.getPrimoMateriale(), aeromodello.getSecondoMateriale(), aeromodello.getTerzoMateriale() } },
            { "Dimensione", aeromodello.getDimensione() },
            { "Costo di produzione", aeromodello.getCostoProduzione() },
            { "Prezzo di vendita", aeromodello.getPrezzoVendita() },
            { "Guadagno", (aeromodello.getPrezzoVendita() - aeromodello.getCostoProduzione()) },
            { "Ha un radiocomando?", aeromodello.getRadiocomandato() },
            { "Numero di ali", aeromodello.getNumAli() },
        };
    }

    // generazione e memorizzazione delle barche
    to_dump["Barche"] = json::array();
    for (int i = 0; i < num_barche; i++) {
        Barca barca = randomBarca();

        to_dump["Barche"][i] = {
            { "Materiali", { barca.getPrimoMateriale(), barca.getSecondoMateriale(), barca.getTerzoMateriale() } },
            { "Dimensione", barca.getDimensione() },
            { "Costo di produzione", barca.getCostoProduzione() },
            { "Prezzo di vendita", barca.getPrezzoVendita() },
            { "Guadagno", (barca.getPrezzoVendita() - barca.getCostoProduzione()) },
            { "Ha un radiocomando?", barca.getRadiocomandato() },
            { "Numero di vele", barca.getnumVele() },
            { "Finitura", barca.getFiniture() }
        };
    }

    // salva to_dump in giocattoli.json
    ofstream output_file("giocattoli.json");
    output_file << setw(4) << to_dump << endl;
}
