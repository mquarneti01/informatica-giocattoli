/* informatica-giocattoli
 *
 * Copyright (C) 2018  Claudio Giordani, Manuel Quarneti, Pietro Pezzi, Raffaele Ceroni
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config_loader.h"

ConfigLoader::ConfigLoader()
{
    // carica il json contenente i valori degli attributi
    ifstream inputFile("config.json");

    // se il file non e' accessibile, lo segnala
    if (!inputFile.good())
        cout << "Inserire config.json nella directory dell'eseguibile" << endl;

    inputFile >> j;
}

// getters delle varie voci della configurazione
int ConfigLoader::getNumPeluches() { return j["num_peluches"]; }
int ConfigLoader::getNumBambole() { return j["num_bambole"]; }
int ConfigLoader::getNumRobots() { return j["num_robots"]; }
int ConfigLoader::getNumAutomobili() { return j["num_automobili"]; }
int ConfigLoader::getNumAeromodelli() { return j["num_aeromodelli"]; }
int ConfigLoader::getNumBarche() { return j["num_barche"]; }
int ConfigLoader::getMaxArti() { return j["max_arti"]; }
int ConfigLoader::getMaxOcchi() { return j["max_occhi"]; }
int ConfigLoader::getMinRuote() { return j["min_ruote"]; }
int ConfigLoader::getMaxRuote() { return j["max_ruote"]; }
int ConfigLoader::getMinAli() { return j["min_ali"]; }
int ConfigLoader::getMaxAli() { return j["max_ali"]; }
int ConfigLoader::getMaxVele() { return j["max_vele"]; }
float ConfigLoader::getMaxCostoProduzione() { return j["max_costo_produzione"]; }
float ConfigLoader::getMaxPrezzoVendita() { return j["max_prezzo_vendita"]; }

int ConfigLoader::getDimensioniSize() { return j["dimensioni"].size(); }
string ConfigLoader::getDimensione(int indice)
{
    return j["dimensioni"][indice];
}

int ConfigLoader::getMaterialiSize() { return j["materiali"].size(); }
string ConfigLoader::getMateriale(int indice)
{
    return j["materiali"][indice];
}

int ConfigLoader::getColoriSize() { return j["colori"].size(); }
string ConfigLoader::getColore(int indice)
{
    return j["colori"][indice];
}

int ConfigLoader::getVestitiSize() { return j["vestiti"].size(); }
string ConfigLoader::getVestito(int indice)
{
    return j["vestiti"][indice];
}

int ConfigLoader::getFinitureSize() { return j["finiture"].size(); }
string ConfigLoader::getFiniture(int indice)
{
    return j["finiture"][indice];
}

ConfigLoader::~ConfigLoader() {}
