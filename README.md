# informatica-giocattoli
Fabbrica di giocattoli

Il progetto è sotto licenza [GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html)

# Compilazione
**Il progetto sarà rilasciato precompilato per GNU/Linux e Windows nella sezione [tags](https://gitlab.com/mquarneti01/informatica-giocattoli/tags)**

È stato deciso di usare il sistema di build [meson](http://mesonbuild.com/), in quanto non crasha appena trova un errore, a differenza di codeblocks. Inoltre il suo utilizzo è meno complicato di cmake.

Per compilare il progetto è necessario meson ed è consigliato l'utilizzo di git:
```
git clone https://gitlab.com/mquarneti01/informatica-giocattoli.git
cd informatica-giocattoli
meson builddir
cd builddir
ninja
```

L'eseguibile sarà in informatica-giocattoli/builddir/src


## Utilizzo
Posizionare config.json nella directory dell'eseguibile e avviare quest'ultimo.

I parametri in config.json possono essere modificati a piacimento.


## Librerie utilizzate
* [nlohmann/json](https://github.com/nlohmann/json)


## Autori
* Manuel Quarneti
* Pietro Pezzi
* Raffaele Ceroni
* Claudio Giordani

## Descrizione programma
### Le sottoclassi e la superclasse
Il programma è formato da nove sottoclassi di una superclasse definita dal nome giocattolo.

Le nove sottoclassi sono le seguenti:
* **Pupazzo**: è una sottoclasse di Giocattolo, definita da due attributi, *numArti* e *numOcchi* e i dovuti metodi per modificare e ottenere il valore degli attributi
* **Peluche**: è una sottoclasse di Pupazzo, definita da un attributo: *haCoda* e i dovuti metodi per modificare e ottenere il valore dell’attributo. Nel costruttore viene definito come materiale di default "stoffa"
* **Bambola**: è una sottoclasse di Pupazzo. Possiede due attributi: *coloreCapelli* e *vestito* e i dovuti metodi per modificare e ottenere il valore degli attributi. Nel costruttore vengono definiti i valori di numOcchi(2) e numArti(4)
* **Robot**: è una sottoclasse di Pupazzo. Possiede l’attributo: *radiocomandato* e i dovuti metodi per modificare e ottenere il valore dell’attributo. Nel costruttore vengono definiti i materiali(PrimoMateriale e secondoMateriale)
* **Modellino**: è una sottoclasse di Giocattolo. Possiede anch’esso l’attributo *radiocomandato* e i dovuti metodi per modificare e ottenere il valore dell’attributo
* **Automobile**: è una sottoclasse di Modellino. Possiede gli attributi: *numRuote* e *colore* e i dovuti metodi per modificare e ottenere il valore degli attributi
* **Aereomodello**: è una sottoclasse di Modellino. Ha *numAli* come attributo e i dovuti metodi per modificare e ottenere il valore dell’attributo
* **Barca**: sottoclasse di Modellino. Possiede gli attributi *numVele* e *finiture* e i dovuti metodi per modificare e ottenere il valore degli attirbuti
* **Puzzle**: è una sottoclasse di Giocattolo caratterizzato dagli attributi *numPezzi* e *soggetto* con i dovuti metodi per modificare e ottenere il valore degli attributi

**N.B.**: Ovviamente tutte le sottoclassi sono provviste di un costruttore e di un distruttore.

Tutte le sottoclassi elencate precedentemente sono sottoclasse dalla superclasse Giocattolo. Quest’ultima possiede 6 attributi: *primoMateriale*, *secondoMateriale*, *terzoMateriale* , *dimensione*, *costoProduzione* e *prezzoVendita*. Ovviamente come tutte le sottoclassi anche la superclasse possiede tutti i metodi per ottenere o modificare il valore di ogni attributo.

### Funzionamento del programma
generatore_giocattoli utilizza la classe ConfigLoader per interfacciarsi con la libreria nlohmann/json, grazie ad essa può leggere il file di configurazione. giocattoli_random si occupa della creazione degli attributi dei giocattoli e dei giocattoli stessi, tutto in maniera casuale (ma come dice il maestro Oogway, il caso non esiste). Dopo aver generato i giocattoli, nel main vengono memorizzati nell'oggetto to_dump (è della classe json) e infine quest'ultimo viene salvato in un file giocattoli.json. (lo standard output può risultare scomodo se vengono generati molti giocattoli)
